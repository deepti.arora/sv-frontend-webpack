import React from 'react';

function App() {
    return (
        <div className="App">
            <h1>Hello Webpack 3: {process.env.NODE_ENV}</h1>
        </div>
    );
}

export default App;
