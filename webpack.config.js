const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

const PACKAGES = [
    'react',
    'react-dom'
];

module.exports = {
    entry: {
        bundle: path.join(__dirname, '/src/index.js'),
        vendor: PACKAGES
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.join(__dirname, '/dist')
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                use: 'babel-loader',
                test: /\.js$/
            }
        ]
    },
    plugins: [
        new htmlWebpackPlugin(
            {
                template: path.join(__dirname, '/src/index.html')
            }
        )
    ],
    devServer: {
        port: process.env.PORT || 3000,
        open: true
    }
};